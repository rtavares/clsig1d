Clsig1D was developed to make it easier to signal processing research and students to synthesize different waveforms, useful for signal analysis studies.



History:
* version 0.1a5   2018/06/21  Added slice vector extractor

* version 0.1a4   2018/06/14  Added gaussian white noise generator

* version 0.1a3   2018/06/13  Added uniform noise generator

* version 0.1a2   2018/06/13  Added DC vector creation

* version 0.1a1    2018/06/01 Square,triangular,sin, random generations
                            Add,subtrac,multiply, concaten operations
                            Visualizer

Information about install and use are in https://cadernodelaboratorio.com.br. 

