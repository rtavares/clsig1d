# !/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#  clsig1d.py
#
#  Copyright 2018-04-13 Roberto Tavares tavares@cadernodelaboratorio.com.br
#
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
import tkinter as tk
import sys
import tkinter.messagebox as tkmsg
import gettext
import configparser
import logging
import os
from pathlib import Path
from os.path import join
import os.path as op

import appData
import aboutDlg
import arbitrary
import square
import sin
import triangular
import pulse
import viewer
import soma
import subtrai
import multiply
import concatena
import autoconcatena
import DC
import uniform
import gaussian
import extract

_ = gettext.gettext


class Clgen(object):

    def __init__(self, **kw):

        try:
            this_file = __file__
        except NameError:
            this_file = sys.argv[0]

        this_file = op.abspath(this_file)
        if getattr(sys, 'frozen', False):
            self.application_path = os.path.dirname(sys.executable)
        else:
            self.application_path = op.dirname(this_file)
        self.homeDir = str(Path.home())
        self.logFile = join(self.homeDir, "clsig1d/clsig1d.log")

        # cria o diretorio se não existe- necessário para o logger
        diretorio_siggen = join(self.homeDir, "clsig1d")

        if not os.path.exists(diretorio_siggen):
            os.makedirs(diretorio_siggen)

        # Create the Logger
        self.logger = logging.getLogger(__name__)
        self.logger.setLevel(logging.INFO)

        # Create the Handler for logging data to a file
        logger_handler = logging.FileHandler(self.logFile, mode='w')
        logger_handler.setLevel(logging.INFO)

        # Create a Formatter for formatting the log messages
        logger_formatter = logging.Formatter('%(name)s - %(levelname)s \
                                             - %(message)s')

        # Add the Formatter to the Handler
        logger_handler.setFormatter(logger_formatter)

        # Add the Handler to the Logger
        self.logger.addHandler(logger_handler)
        self.logger.info(_('Logger OK'))

        # self.dirn = os.path.dirname(os.path.realpath(sys.argv[0]))
        # print(self.dirn)

        inifile = join(self.homeDir, "clsig1d/clsig1d.ini")

        msginifile = "infile = %s" % (inifile)
        self.logger.info(msginifile)

        msgpath = "app path = %s" % (self.application_path)
        self.logger.info(msgpath)

        if not os.path.isfile(inifile):
            # os.makedirs(os.path.dirname(inifile))
            with open(inifile, "w") as f:
                f.write("[LastRun]\n")
                f.write("idiom = pt_BR\n")

        parser = configparser.ConfigParser()
        parser.read(inifile)
        idioma = parser.get('LastRun', 'idiom')
        self.set_language(idioma)

        msgidioma = "idioma = %s" % (idioma)
        self.logger.info(msgidioma)

        msgt = "%s  %s" % (appData.__application__, appData.__version__)

        self.root = tk.Tk()
        self.root.title(msgt)
        self.root.geometry('860x150')

        self.create_menu_bar()
        self.create_canvas_area()

    def create_canvas_area(self):
        pass

    def execute(self):
        self.root.mainloop()

    def create_menu_bar(self):
        menubar = tk.Menu(self.root)

        filemenu = tk.Menu(menubar, tearoff=0)
        menubar.add_cascade(label=_("File"), menu=filemenu)
        filemenu.add_command(label=_("Exit"), command=self.mnuexit)

        configmenu = tk.Menu(menubar, tearoff=0)
        menubar.add_cascade(label=_("Config"), menu=configmenu)

        submenuconfig = tk.Menu(configmenu, tearoff=0)
        configmenu.add_cascade(label=_("Language"),
                               menu=submenuconfig)

        submenuconfig.add_command(label=_("Portuguese"),
                                  command=self.mnuportuguese)
        submenuconfig.add_command(label=_("English"),
                                  command=self.mnuenglish)

        waveformmenu = tk.Menu(menubar, tearoff=0)
        menubar.add_cascade(label=_("Generator"), menu=waveformmenu)

        waveformmenu.add_command(label=_("Square wave"),
                                 command=self.mnusquare)
        waveformmenu.add_command(label=_("Sin wave"),
                                 command=self.mnusin)
        waveformmenu.add_command(label=_("Triangular wave"),
                                 command=self.mnutri)
        waveformmenu.add_command(label=_("Pulse wave"),
                                 command=self.mnupulse)
        waveformmenu.add_command(label=_("Arbitrary"),
                                 command=self.mnuarbitrary)
        waveformmenu.add_command(label=_("DC"),
                                 command=self.mnudc)

        noisemenu = tk.Menu(waveformmenu, tearoff=0)
        waveformmenu.add_cascade(label=_("Noise"), menu=noisemenu)

        noisemenu.add_command(label=_("Uniform"),
                              command=self.mnuuniformnoise)
        noisemenu.add_command(label=_("Gaussian"),
                              command=self.mnugaussiannoise)

        mathmenu = tk.Menu(menubar, tearoff=0)
        menubar.add_cascade(label=_("Math"), menu=mathmenu)

        mathmenu.add_command(label=_("Viewer"),
                             command=self.mnuvisualizador)
        mathmenu.add_command(label=_("Add"), command=self.mnusoma)
        mathmenu.add_command(label=_("Subtract"),
                             command=self.mnusubtrai)
        mathmenu.add_command(label=_("Multiply"),
                             command=self.mnumultiply)
        mathmenu.add_command(label=_("Concatenate"),
                             command=self.mnuconcatena)
        mathmenu.add_command(label=_("Self concatenate"),
                             command=self.mnuautoconcatena)
        mathmenu.add_command(label=_("Extract segment"),
                             command=self.mnuextractsegment)

        helpmenu = tk.Menu(menubar, tearoff=0)
        helpmenu.add_command(label=_("About"), command=self.mnuabout)
        menubar.add_cascade(label=_("Help"), menu=helpmenu)

        self.root.config(menu=menubar)

    def mnuexit(self):
        self.root.destroy()

    def mnuabout(self):
        dlg = aboutDlg.AboutDlg(parent=self.root, title=_("About Dlg"))  # NOQA

    def change_language(self, language):
        self.idiom = language
        config = configparser.ConfigParser()

        config['LastRun'] = {}
        config['LastRun']['idiom'] = language

        arquivo_ini = join(self.homeDir, "clsig1d/clsig1d.ini")

        with open(arquivo_ini, 'w') as configfile:
            config.write(configfile)

        self.set_language(language)

        tkmsg.showwarning(_("Warning"),
                          _("Please restart app to modifications \
                            take effect"))

    def set_language(self, language):
        locale_dir = join(self.application_path, 'locales')

        msglocaledir = "locale dir = %s" % (locale_dir)
        self.logger.info(msglocaledir)

        if language == "pt_BR":
            gettext.bindtextdomain('pt_BR', locale_dir)
            gettext.textdomain('pt_BR')

    def mnuportuguese(self):
        self.change_language("pt_BR")

    def mnuenglish(self):
        self.change_language("en")

    def mnutri(self):
        """
        gera onda triangular
        """
        dlg = triangular.Triangular(parent=self.root, title=_("Triangular wave generation"))  # NOQA

    def mnusin(self):
        """
        gera onda senoidal
        """
        dlg = sin.Sin(parent=self.root, title=_("Sin wave generation"))  # NOQA

    def mnusquare(self):
        """
        gera uma onda quadrada
        """
        dlg = square.Square(parent=self.root, title=_("Square wave generation"))  # NOQA

    def mnuarbitrary(self):
        """
        gera uma onda arbitraria
        """
        dlg = arbitrary.Arbitrary(parent=self.root, title=_("Arbitrary wave generation"))  # NOQA

    def mnudc(self):
        """
        gera um  sinal dc
        """
        dlg = DC.Dc(parent=self.root, title=_("Constant level sgnal  generation"))  # NOQA

    def mnupulse(self):
        """
        gera pulso
        """
        dlg = pulse.Pulse(parent=self.root, title=_("Pulse wave generation"))  # NOQA

    def mnuvisualizador(self):
        dlg = viewer.Viewer(parent=self.root, title=_("Signal Viewer"))  # NOQA

    def mnusoma(self):
        dlg = soma.Soma(parent=self.root, title=_("Add signals"))  # NOQA

    def mnusubtrai(self):
        dlg = subtrai.Subtrai(parent=self.root, title=_("Subtract signals"))  # NOQA

    def mnuconcatena(self):
        dlg = concatena.Concatena(parent=self.root, title=_("Concatenate signals"))  # NOQA

    def mnuautoconcatena(self):
        dlg = autoconcatena.Autoconcatena(parent=self.root, title=_("Self concatenate signal"))  # NOQA

    def mnumultiply(self):
        dlg = multiply.Multiply(parent=self.root, title=_("Multiply signals"))  # NOQA

    def mnuuniformnoise(self):
        dlg = uniform.Uniform(parent=self.root, title=_("Uniform white noise  generator"))  # NOQA

    def mnugaussiannoise(self):
        dlg = gaussian.Gaussian(parent=self.root, title=_("Gaussian white noise  generator"))  # NOQA

    def mnuextractsegment(self):
        dlg = extract.Extract(parent=self.root, title=_("Extract signal segment"))  # NOQA


def main(args):
    my_app = Clgen()
    my_app.execute()
    return 0


if __name__ == '__main__':
    sys.exit(main(sys.argv))
