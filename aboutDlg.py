#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#  aboutDlg.py
#
#  Copyright 2018-04-13 Roberto Tavares tavares@cadernodelaboratorio.com.br
#  Original source code  got in Internet, but I lost this information
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
import os
import sys
from os.path import join
from sys import version
import tkinter as tk
import appData
try:
    from idlelib import textView
except ImportError:
    from idlelib import textview

import gettext
_ = gettext.gettext


class AboutDlg(tk.Toplevel):
    """
    Diálogo about padrão das aplicações do caderno de laboratório
    """
    def __init__(self, parent, title):

        tk.Toplevel.__init__(self, parent)
        self.configure(borderwidth=5)

        self.geometry("+%d+%d" % (
                        parent.winfo_rootx()+30,
                        parent.winfo_rooty()+100))

        self.bg = "#707070"
        self.fg = "#ffffff"

        self.dirn = os.path.dirname(os.path.realpath(sys.argv[0]))
        self.arquivo_licensa = join(self.dirn, "license.txt")
        self.arquivo_copyright = join(self.dirn, "copyright.txt")
        self.arquivo_creditos = join(self.dirn, "creditos.txt")
        self.arquivo_translators = join(self.dirn, "translators.txt")
        self.arquivo_readme = join(self.dirn, "readme.txt")
        self.arquivo_news = join(self.dirn, "news.txt")

        self.create_widgets()

        self.resizable(height=tk.FALSE, width=tk.FALSE)

        self.title(title)
        self.transient(parent)
        self.grab_set()
        self.protocol("WM_DELETE_WINDOW", self.ok)
        self.parent = parent
        self.buttonOk.focus_set()
        self.bind('<Return>', self.ok)
        self.bind('<Escape>', self.ok)  # dismiss dialog
        self.wait_window()

    def create_widgets(self):
        release = version[:version.index(' ')]   # versão do python

        frame_main = tk.Frame(self, borderwidth=2, relief=tk.SUNKEN)

        frame_buttons = tk.Frame(self)
        frame_buttons.pack(side=tk.BOTTOM, fill=tk.X)

        frame_main.pack(side=tk.TOP, expand=tk.TRUE, fill=tk.BOTH)

        self.buttonOk = tk.Button(frame_buttons, text='Close',
                                  command=self.ok)

        self.buttonOk.pack(padx=5, pady=5)

        frame_bg = tk.Frame(frame_main, bg=self.bg)
        frame_bg.pack(expand=tk.TRUE, fill=tk.BOTH)

        frame_title = appData.__application__ + " v. " + appData.__version__

        label_title = tk.Label(frame_bg, text=frame_title,
                               fg=self.fg, bg=self.bg,
                               font=('courier', 24, 'bold'))

        label_title.grid(row=0, column=0, sticky=tk.W, padx=10, pady=10)

        label_desc = tk.Label(frame_bg,
                              text=_(appData.__shortDescriptition__),
                              justify=tk.LEFT,
                              fg=self.fg, bg=self.bg)
        label_desc.grid(row=2, column=0, sticky=tk.W, columnspan=3,
                        padx=10, pady=5)
        label_email = tk.Label(frame_bg, text=appData.__email__,
                               justify=tk.LEFT, fg=self.fg, bg=self.bg)
        label_email.grid(row=6, column=0, columnspan=2,
                         sticky=tk.W, padx=10, pady=0)
        label_www = tk.Label(frame_bg,
                             text=appData.__site__,
                             justify=tk.LEFT, fg=self.fg, bg=self.bg)
        label_www.grid(row=7, column=0, columnspan=2, sticky=tk.W,
                       padx=10, pady=0)
        tk.Frame(frame_bg, borderwidth=1, relief=tk.SUNKEN,
                 height=2, bg=self.bg).grid(row=8, column=0,
                                            sticky=tk.EW,
                                            columnspan=3,
                                            padx=5, pady=5)

        label_python_ver = tk.Label(frame_bg,
                                    text=_('Python version:  ') +
                                    release, fg=self.fg, bg=self.bg)
        label_python_ver.grid(row=9, column=0, sticky=tk.W, padx=10,
                              pady=0)

        tk_ver = self.tk.call('info', 'patchlevel')
        labeltk_ver = tk.Label(frame_bg, text='Tk version:  ' +
                               tk_ver, fg=self.fg, bg=self.bg)
        labeltk_ver.grid(row=9, column=1, sticky=tk.W, padx=2, pady=0)

        tk_button_f = tk.Frame(frame_bg, bg=self.bg)
        tk_button_f.grid(row=10, column=0, columnspan=2, sticky=tk.NSEW)

        button_license = tk.Button(tk_button_f, text=_('License'), width=8,
                                   highlightbackground=self.bg,
                                   command=self.show_license)
        button_license.pack(side=tk.LEFT, padx=10, pady=10)

        button_copyright = tk.Button(tk_button_f, text=_('Copyright'), width=8,
                                     highlightbackground=self.bg,
                                     command=self.show_copyright)
        button_copyright.pack(side=tk.LEFT, padx=10, pady=10)

        button_credits = tk.Button(tk_button_f, text=_('Credits'), width=8,
                                   highlightbackground=self.bg,
                                   command=self.show_python_credits)
        button_credits.pack(side=tk.LEFT, padx=10, pady=10)

        tk.Frame(frame_bg, borderwidth=1, relief=tk.SUNKEN,
                 height=2, bg=self.bg).grid(row=11,
                                            column=0, sticky=tk.EW,
                                            columnspan=3, padx=5,
                                            pady=5)

        idle_button_f = tk.Frame(frame_bg, bg=self.bg)
        idle_button_f.grid(row=13, column=0, columnspan=3,
                           sticky=tk.NSEW)

        readme_b = tk.Button(idle_button_f, text=_('Readme'), width=8,
                             highlightbackground=self.bg,
                             command=self.show_readme)
        readme_b.pack(side=tk.LEFT, padx=10, pady=10)

        news_b = tk.Button(idle_button_f, text=_('News'), width=8,
                           highlightbackground=self.bg,
                           command=self.show_news)
        news_b.pack(side=tk.LEFT, padx=10, pady=10)

        translators_b = tk.Button(idle_button_f, text=_('Translators'),
                                  width=8,
                                  highlightbackground=self.bg,
                                  command=self.show_translators)
        translators_b.pack(side=tk.LEFT, padx=10, pady=10)

    def show_license(self):
        self.display_file_text(_('About - License'),
                               self.arquivo_licensa, 'utf-8')

    def show_copyright(self):
        self.display_file_text(_('About - Copyright'),
                               self.arquivo_copyright, 'utf-8')

    def show_python_credits(self):
        self.display_file_text(_('About - Credits'),
                               self.arquivo_creditos, 'utf-8')

    def show_translators(self):
        self.display_file_text(_('About - Translator'),
                               self.arquivo_translators, 'utf-8')

    def show_readme(self):
        self.display_file_text(_('About - Readme'), self.arquivo_readme,
                               'utf-8')

    def show_news(self):
        self.display_file_text(_('About - News'),
                               self.arquivo_news, 'utf-8')

    def display_printer_text(self, title, printer):
        printer._Printer__setup()
        text = '\n'.join(printer._Printer__lines)
        try:
            textView.view_text(self, title, text)
        except (RuntimeError, TypeError, NameError):
            textview.view_text(self, title, text)

    def display_file_text(self, title, filename, encoding=None):
        fn = os.path.join(os.path.abspath(os.path.dirname(__file__)),
                          filename)
        try:
            textView.view_file(self, title, fn, encoding)
        except (RuntimeError, TypeError, NameError):
            textview.view_file(self, title, fn, encoding)

    def ok(self, event=None):
        self.destroy()
