# !/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  gausian.py
#  Copyright 2018-05-03 tavares <tavares@tavares-Inspiron-5558>
#  1.0
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#  ATENÇÂO. A versão do PILLOW DEVE SER a 3.1.2- Versões superiores
#  não estão funcionando
#
# O vetor de dados correspodente ao sinal lido é um vetor numpy
# denominado self.vetor_pontos

import tksimpledialog
import tkinter as tk
import os
import logging
import sys
from pathlib import Path
from os.path import join
import gettext
import numpy as np

_ = gettext.gettext


class Gaussian(tksimpledialog.Dialog):
    """
    Generate a gaussian white noise vector
    """

    def __init__(self, parent, title=None):
        tksimpledialog.Dialog.__init__(self, parent, title, numero_botoes=1)

    def body(self, master):
        # create dialog body.  return widget that should have
        # initial focus.  this method should be overridden

        if getattr(sys, 'frozen', False):
            # If the application is run as a bundle, the pyInstaller bootloader
            # extends the sys module by a flag frozen=True and sets the app
            # path into variable _MEIPASS'.
            application_path = sys._MEIPASS  # NOQA
        else:
            application_path = os.path.dirname(os.path.abspath(__file__))  # NOQA

        self.homeDir = str(Path.home())
        self.logFile = join(self.homeDir, "clsig1d/clsig1d.log")

        # Create the Logger
        self.logger = logging.getLogger(__name__)
        self.logger.setLevel(logging.INFO)

        # Create the Handler for logging data to a file
        logger_handler = logging.FileHandler(self.logFile)
        logger_handler.setLevel(logging.INFO)

        # Create a Formatter for formatting the log messages
        logger_formatter = \
            logging.Formatter('%(name)s - %(levelname)s - %(message)s')

        # Add the Formatter to the Handler
        logger_handler.setFormatter(logger_formatter)

        # Add the Handler to the Logger
        self.logger.addHandler(logger_handler)
        self.logger.info(_('Entering module'))

        # ----------------
        box0 = tk.Frame(self)
        self.lbl0 = tk.Label(box0, text=_("Gaussian white noise  generator"),
                             fg="blue", font=("Helvetica", 16))
        self.lbl0.pack()
        box0.pack()

        # ------------------------------

        box2 = tk.Frame(self)

        self.points = tk.StringVar()
        self.points.set("128")
        lbl2points = tk.Label(box2, text=_("Total points:"),
                              padx=10, pady=10)
        lbl2points.grid(row=0, column=0, padx=5, pady=5)
        ent2points = tk.Entry(box2, textvariable=self.points,
                              width=5)
        ent2points.grid(row=0, column=1, padx=1, pady=5)

        self.valormedio = tk.StringVar()
        self.valormedio.set("0.0")
        lbl2valormedio = tk.Label(box2, text=_("Average:"),
                                  padx=10, pady=10)
        lbl2valormedio.grid(row=0, column=3, padx=5, pady=5)
        ent2valormedio = tk.Entry(box2, textvariable=self.valormedio,
                                  width=5)
        ent2valormedio.grid(row=0, column=4, padx=1, pady=5)

        self.desviopadrao = tk.StringVar()
        self.desviopadrao.set("0.5")
        lbl2desviopadrao = tk.Label(box2, text=_("Std deviation:"),
                                    padx=10, pady=10)
        lbl2desviopadrao.grid(row=0, column=6, padx=5, pady=5)
        ent2desviopadrao = tk.Entry(box2, textvariable=self.desviopadrao,
                                    width=5)
        ent2desviopadrao.grid(row=0, column=7, padx=1, pady=5)
        # ----------------------------------------------

        box3 = tk.Frame(self, bd=2, padx=10, pady=10, relief=tk.RAISED)

        self.btnStart = tk.Button(box3, text=_("Generate and save file"),
                                  command=self.start_job)
        self.btnStart.grid(row=0, column=2, padx=5, pady=5)
        box2.pack()
        box3.pack()

        return box2

    def start_job(self):
        """
        """
        pontos = int(self.points.get())
        media = float(self.valormedio.get())
        desvio = float(self.desviopadrao.get())
        self.vetor_pontos = self.gera_gaussiano(numero_amostras=pontos,
                                                valor_medio=media,
                                                desvio_padrao=desvio)
        self.salva_curva()

    def salva_curva(self):

        # Ask the user to select a single file name for saving.
        my_filetypes = [(_('csv files'), '.csv'),
                        (_('text files'), '.txt'),
                        (_('all files'), '.*')]
        msgt = _("Please select a file name for saving:")
        arquivo = tk.filedialog.asksaveasfilename(parent=self,
                                                  initialdir=os.getcwd(),
                                                  title=msgt,
                                                  filetypes=my_filetypes)

        if arquivo:
            np.savetxt(arquivo, self.vetor_pontos, newline="\n")

    def gera_gaussiano(self, numero_amostras=1024,
                       valor_medio=0.0, desvio_padrao=0.5):
        """
        Gera um array numpy correspondente ao ruído branco gaussiano
        """
        sinal = np.random.normal(loc=valor_medio, scale=desvio_padrao,
                                 size=numero_amostras)
        return sinal
