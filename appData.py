# !/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  appData.py
#  Copyright 2018-05-06 tavares <tavares@tavares-Inspiron-5558>
#  1.0
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#
import gettext

_ = gettext.gettext

__version__ = "0.1a6"
__application__ = "clsig1d"
__shortDescriptition__ = _("Cl Signal Processing Visual Calculator")
__author__ = "Roberto Tavares"
__site__ = "www.cadernodelaboratorio.com.br"
__licensa__ = "GPL"
__email__ = "tavares@cadernodelaboratorio.com.br"

__logging__ = True
