#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  viewer.py
#  Copyright 2018-05-03 tavares <tavares@tavares-Inspiron-5558>
#  1.0
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#  ATENÇÂO. A versão do PILLOW DEVE SER a 3.1.2-
# Versões superiores não estão funcionando
#
# O vetor de dados correspodente ao sinal lido é um vetor numpy
# denominado self.veto_pontos

import tksimpledialog
import tkinter as tk
import os
import logging
import sys
from pathlib import Path
from os.path import join
import gettext
import numpy as np
import matplotlib
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
from matplotlib.backends.backend_tkagg import NavigationToolbar2Tk

from matplotlib.figure import Figure

matplotlib.use('TkAgg')

_ = gettext.gettext


class Viewer(tksimpledialog.Dialog):

    def __init__(self, parent, title=None):
        tksimpledialog.Dialog.__init__(self, parent,
                                       title, numero_botoes=1)

    def body(self, master):
        # create dialog body.  return widget that should have
        # initial focus.  this method should be overridden

        if getattr(sys, 'frozen', False):
            # If the application is run as a bundle,
            # the pyInstaller bootloader
            # extends the sys module by a flag frozen=True and
            # sets the app
            # path into variable _MEIPASS'.
            application_path = sys._MEIPASS  # NOQA
        else:
            application_path = os.path.dirname(os.path.abspath(__file__))  # NOQA

        self.homeDir = str(Path.home())
        self.logFile = join(self.homeDir, "clsig1d/clsig1d.log")

        # Create the Logger
        self.logger = logging.getLogger(__name__)
        self.logger.setLevel(logging.INFO)

        # Create the Handler for logging data to a file
        logger_handler = logging.FileHandler(self.logFile)
        logger_handler.setLevel(logging.INFO)

        # Create a Formatter for formatting the log messages
        logger_formatter = \
            logging.Formatter('%(name)s - %(levelname)s - %(message)s')

        # Add the Formatter to the Handler
        logger_handler.setFormatter(logger_formatter)

        # Add the Handler to the Logger
        self.logger.addHandler(logger_handler)
        self.logger.info(_('Entering module'))

        # ----------------
        box0 = tk.Frame(self)
        self.lbl0 = tk.Label(box0, text=_("Signal viewer"), fg="blue",
                             font=("Helvetica", 16))
        self.lbl0.pack()
        box0.pack()
        # ----------------------------------------------

        box3 = tk.Frame(self, bd=2, padx=10, pady=10, relief=tk.RAISED)

        self.btnLoad = tk.Button(box3, text=_("Load file"),
                                 command=self.le_curva)
        self.btnLoad.grid(row=0, column=4, padx=2, pady=2)

        box3.pack()

        return self.btnLoad

    def le_curva(self):
        # Ask the user to select a single file name for saving.
        # Build a list of tuples for each file type the file dialog
        # should display
        my_filetypes = [(_('csv files'), '.csv'),
                        (_('text files'), '.txt'),
                        (_('all files'), '.*')]

        msg_title = _("Please select a file name for load:")
        arquivo = tk.filedialog.askopenfilename(parent=self,
                                                initialdir=os.getcwd(),
                                                title=msg_title,
                                                filetypes=my_filetypes)

        if arquivo:
            self.vetor_pontos = np.loadtxt(arquivo, delimiter=',')
            self.show_curve(self.vetor_pontos)

    def show_curve(self, list_vector):
        tamanho = list_vector.size
        tempo = np.arange(0.0, tamanho, 1)

        figura = Figure(figsize=(5, 4), dpi=100)
        area = figura.add_subplot(111)
        area.plot(tempo, list_vector)

        canvas = FigureCanvasTkAgg(figura, master=self)
        canvas.draw()
        canvas.get_tk_widget().pack(side=tk.TOP, fill=tk.BOTH, expand=1)

        toolbar = NavigationToolbar2Tk(canvas, self)
        toolbar.update()
        canvas._tkcanvas.pack(side=tk.TOP, fill=tk.BOTH, expand=1)
