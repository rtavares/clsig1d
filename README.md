# clsig1d

Clsig1d is a pure Python (3.5)  software designed to help in signal processing research. The objective is allow the use of sophisticated algorithms without worrying about maths aspects. Focus its operations in  time series that can be represented by 1D vectors.

It needs the following libs:

* tkinter
* numpy 
* matplotlib




## Operation systems

Tested in Linux Mint 18.1, MATE interface, 64 bits  and Windows 7

To execute:  python clsig1d.py


## Information about versions:

* version 0.1a5   2018/06/21  Added slice vector extractor

* version 0.1a4   2018/06/14  Added gaussian white noise generator

* version 0.1a3   2018/06/13  Added uniform noise generator

* version 0.1a2   2018/06/13  Added DC vector creation

* version 0.1a1    2018/06/01 Square,triangular,sin, random generations
                            Add,subtrac,multiply, concaten operations
                            Visualizer



## Detailed instructions

Portuguese instructions can be found in https://cadernodelaboratorio.com.br

We realize that automatic translated services nowdays are terrible. Non portuguese speakers can have a hard time to understand parts of the translated text. If this is your case, you can ask us in english about anything in the blog. We will answer also in english.

Nos damos cuenta de que los textos traducidos automáticamente tienen sus limitaciones. Los que no hablan portugues pueden tener dificultades para entender parte del texto traducido. Si este es su caso, no dude en preguntarnos en español sobre cualquier informacion del blog. Responderemos también en español (or portuñol, estoy seguro que podremos llegar a entendernos).:-) ).
