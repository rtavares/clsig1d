��    X      �     �      �     �     �     �     �     �     �     �     �  	               
     	   )     3     P     j     q     }     �  	   �     �  
   �     �     �     �     �  
   �     �     �     �  	   	     	     $	     -	  	   5	  	   ?	     I	     V	  
   [	  
   f	     q	     z	     �	     �	  /   �	  #   �	  %   �	     
  
   
      
     /
  
   =
     H
     ^
     s
     �
     �
     �
  	   �
     �
     �
     �
     �
               $     -     A     T     `     w  	   �     �     �     �     �     �     �     �     �                '     .  	   6  	   @  
   J  �  U               %     4     D     R     b     o  	   ~     �     �     �     �     �     �     �  	   �       	     	   #  	   -  	   7     A     I     ]     b     j     x     �     �     �     �     �     �     �  	   �     �     �            
   %     0  	   C     M  !   P  4   r  ,   �     �  
   �     �     �                    .     >     E     Y     q     �     �  !   �     �     �     �               7     P     ^     w     �     �     �     �     �     �  
   �     �                3     :  	   G     Q     `     m     Q       R   P   ,   $   9                   F       5                <   &   B   S                    !       T       -   .      
           V   G   M       0       X   1      A         D   2             E   :   W               "                                 O       H   U   7   @          J      *   ?       	          +   /      >       6             N       ;   8       3   L      I   K   #   '               =   )                      C          4   (   %               ... About About - Copyright About - Credits About - License About - News About - Readme About - Translator About Dlg Add Add signals Amplitude: Arbitrary Arbitrary function generator Arbitrary wave generation Cancel Concatenate Concatenate signals Config Copyright Credits DC level:  English Entering module Exit File File saved First File: Frequency (Hz): Generate and save file Generator Help Language License Load file Logger OK Mark/space:  Math Max value: Min value: Multiply Multiply signals News OK Please restart app to modifications take effect Please select a file name for load: Please select a file name for saving: Points: Portuguese Present point: Product file: Pulse wave Pulse wave generation Pulse wave generator Python version:   Readme Result file: Sample rate: (s) Save file Second file: Self concatenate Self concatenate signal Signal Generator File Creator Signal Viewer Signal viewer Sin wave Sin wave generation Sin wave generator Square wave Square wave generation Square wave generator Start Job Subtract Subtract signals Success Times:  Total points: Translators Triangular wave Triangular wave generation Triangular wave generator Value: Viewer Warning all files csv files text files Project-Id-Version: SigGen 0.1a1
POT-Creation-Date: 2018-06-01 08:20-0300
PO-Revision-Date: 2018-06-01 08:21-0300
Last-Translator: Roberto Tavares <tavares@cadernodelaboratorio.com.br>
Language-Team: 
Language: pt_BR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.8.7.1
X-Poedit-Basepath: .
Plural-Forms: nplurals=2; plural=(n > 1);
X-Poedit-SourceCharset: UTF-8
X-Poedit-SearchPath-0: .
 ... Sobre SobreCopyright Sobre-Créditos Sobre-Licensa Sobre-Novidades Sobre-Leiame Sobre-Tradutor Sobre dlg Soma Soma sinais Amplitude (pp): Onda arbitrária Gerador de função arbitrária Gerador de onda arbitrária Cancela Concatena Concatena sinais Configura Copyright Créditos Nivel DC: Inglês Entrando no módulo Sair Arquivo Arquivo salvo Primeiro arquivo: Frequência (Hz): Gerar  e salvar arquivo Gerador Ajuda Idioma Licensa Carregar arquivo Logger OK Marca/espaço: Matemática Valor máximo: Valor mínimo: Multiplica Multiplica sinais: Novidades OK Por favor reinicie a aplicação! Por favor selecione o nome do arquivo para carregar: Por favor selecione nome para salvar arquivo Pontos: Português Ponto atual: Arquivo produto: Pulso Gerador de pulso Gerador de pulsos Versão Python: Leiame Arquivo resultante: Taxa de amostragem (s): Salvar arquivo Segundo arquivo: Auto concatena Concatena o arquivo com ele mesmo Gerador de sinais para arquivos Visualizador de sinais Visualizador de sinais Onda senoidal Gerador de onda senoidal Gerador de onda senoidal Onda quadrada Gerador de onda quadrada Gerador de onda quadrada Iniciar trabalho Subtrai Subtrai sinais Sucesso Vezes a concatenar: Total de pontos: Tradutores Onda triangular Gerador onda triangular Gerador de onda triangular Valor: Visualizador Atenção todos arquivos arquivos csv arquivos texto 