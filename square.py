#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  square.py
#  Copyright 2018-05-03 tavares <tavares@tavares-Inspiron-5558>
#  1.0
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#  ATENÇÂO. A versão do PILLOW DEVE SER a 3.1.2- Versões superiores
# não estão funcionando
#
# O vetor de dados correspodente ao sinal lido é um vetor numpy
# denominado self.vetor_pontos

import tksimpledialog
import tkinter as tk
import os
import logging
import sys
from pathlib import Path
from os.path import join
import gettext
import numpy as np
from scipy import signal
from tkinter.filedialog import asksaveasfilename

_ = gettext.gettext


class Square(tksimpledialog.Dialog):

    def __init__(self, parent, title=None):
        tksimpledialog.Dialog.__init__(self, parent, title, numero_botoes=1)

    def body(self, master):
        # create dialog body.  return widget that should have
        # initial focus.  this method should be overridden

        if getattr(sys, 'frozen', False):
            # If the application is run as a bundle, the pyInstaller bootloader
            # extends the sys module by a flag frozen=True and sets the app
            # path into variable _MEIPASS'.
            application_path = sys._MEIPASS  # NOQA
        else:
            application_path = os.path.dirname(os.path.abspath(__file__))  # NOQA

        self.homeDir = str(Path.home())
        self.logFile = join(self.homeDir, "clsig1d/clsig1d.log")

        # Create the Logger
        self.logger = logging.getLogger(__name__)
        self.logger.setLevel(logging.INFO)

        # Create the Handler for logging data to a file
        logger_handler = logging.FileHandler(self.logFile)
        logger_handler.setLevel(logging.INFO)

        # Create a Formatter for formatting the log messages
        logger_formatter = \
            logging.Formatter('%(name)s - %(levelname)s - %(message)s')

        # Add the Formatter to the Handler
        logger_handler.setFormatter(logger_formatter)

        # Add the Handler to the Logger
        self.logger.addHandler(logger_handler)
        self.logger.info(_('Entering module'))

        # ----------------
        box0 = tk.Frame(self)
        self.lbl0 = tk.Label(box0, text=_("Square wave generator"),
                             fg="blue", font=("Helvetica", 16))
        self.lbl0.pack()
        box0.pack()

        # ------------------------------

        box2 = tk.Frame(self)

        self.points = tk.StringVar()
        self.points.set("2048")
        lbl2points = tk.Label(box2, text=_("Total points:"),
                              padx=10, pady=10)
        lbl2points.grid(row=0, column=0, padx=5, pady=5)
        ent2points = tk.Entry(box2, textvariable=self.points,
                              width=5)
        ent2points.grid(row=0, column=1, padx=1, pady=5)

        self.freq = tk.StringVar()
        self.freq.set("1000")
        lbl2freq = tk.Label(box2, text=_("Frequency (Hz):"),
                            padx=10, pady=10)
        lbl2freq.grid(row=0, column=2, padx=5, pady=5)
        ent2freq = tk.Entry(box2, textvariable=self.freq, width=5)
        ent2freq.grid(row=0, column=3, padx=1, pady=5)

        self.amplitude = tk.StringVar()
        self.amplitude.set("5")
        lbl2amplitude = tk.Label(box2, text=_("Amplitude:"),
                                 padx=10, pady=10)
        lbl2amplitude.grid(row=0, column=4, padx=5, pady=5)
        ent2amplitude = tk.Entry(box2, textvariable=self.amplitude,
                                 width=5)
        ent2amplitude.grid(row=0, column=5, padx=1, pady=5)

        self.sampleRate = tk.StringVar()
        self.sampleRate.set("0.00001")
        lbl2sample_rate = tk.Label(box2, text=_("Sample rate: (s)"),
                                   padx=10, pady=10)
        lbl2sample_rate.grid(row=1, column=0, padx=5, pady=5)
        ent2sample_rate = tk.Entry(box2, textvariable=self.sampleRate,
                                   width=10)
        ent2sample_rate.grid(row=1, column=1, padx=1, pady=5)

        self.nivelDC = tk.StringVar()
        self.nivelDC.set("0.0")
        lbl2_niveldc = tk.Label(box2, text=_("DC level: "),
                                padx=10, pady=10)
        lbl2_niveldc.grid(row=1, column=3, padx=5, pady=5)
        ent2_niveldc = tk.Entry(box2, textvariable=self.nivelDC,
                                width=10)
        ent2_niveldc.grid(row=1, column=4, padx=1, pady=5)

        # ----------------------------------------------

        box3 = tk.Frame(self, bd=2, padx=10, pady=10, relief=tk.RAISED)

        self.btnStart = tk.Button(box3, text=_("Generate and save file"),
                                  command=self.start_job)
        self.btnStart.grid(row=0, column=2, padx=5, pady=5)

        box2.pack()
        box3.pack()

        return box2

    def start_job(self):
        """

        """
        pontos = int(self.points.get())
        frequencia = float(self.freq.get())
        taxa_amostragem = float(self.sampleRate.get())
        ampli = float(self.amplitude.get())
        dc = float(self.nivelDC.get())
        self.vetor_pontos = self.gera_quadrado(frequencia, pontos, ampli,
                                               taxa_amostragem, dc)
        self.salva_curva()

    def salva_curva(self):

        # Ask the user to select a single file name for saving.
        my_filetypes = [(_('csv files'), '.csv'),
                        (_('text files'), '.txt'),
                        (_('all files'), '.*')]
        msgt = _("Please select a file name for saving:")
        arquivo = asksaveasfilename(parent=self,
                                    initialdir=os.getcwd(),
                                    title=msgt,
                                    filetypes=my_filetypes)

        if arquivo:
            np.savetxt(arquivo, self.vetor_pontos, newline="\n")

    def gera_quadrado(self, frequencia=1000.0, numero_amostras=1024,
                      valor_pico=1.0, taxa_amostragem=0.000001,
                      nivel_dc=0):
        """
        Gera um array numpy correspondente a uma forma de onda quadrada
        """

        tempo = np.arange(start=0, stop=numero_amostras*taxa_amostragem,
                          step=taxa_amostragem, dtype=np.float)
        quadrado = signal.square(2 * np.pi * frequencia * tempo) \
            * valor_pico + nivel_dc

        return quadrado
