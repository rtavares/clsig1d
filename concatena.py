# !/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  concatena.py
#  Copyright 2018-05-03 tavares <tavares@tavares-Inspiron-5558>
#  1.0
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#  ATENÇÂO. A versão do PILLOW DEVE SER a 3.1.2- Versões
# superiores não estão funcionando
#
# O vetor de dados correspodente ao sinal lido é um vetor numpy
# denominado self.vetor_pontos

import tksimpledialog
import tkinter as tk
import os
import logging
import sys
from pathlib import Path
from os.path import join
import gettext
import numpy as np

_ = gettext.gettext


class Concatena(tksimpledialog.Dialog):

    def __init__(self, parent, title=None):
        tksimpledialog.Dialog.__init__(self, parent, title, numero_botoes=1)

    def body(self, master):
        # create dialog body.  return widget that should have
        # initial focus.  this method should be overridden

        if getattr(sys, 'frozen', False):
            # If the application is run as a bundle, the pyInstaller bootloader
            # extends the sys module by a flag frozen=True and sets the app
            # path into variable _MEIPASS'.
            application_path = sys._MEIPASS  # NOQA
        else:
            application_path = os.path.dirname(os.path.abspath(__file__))  # NOQA

        self.homeDir = str(Path.home())
        self.logFile = join(self.homeDir, "clsig1d/clsig1d.log")

        # Create the Logger
        self.logger = logging.getLogger(__name__)
        self.logger.setLevel(logging.INFO)

        # Create the Handler for logging data to a file
        logger_handler = logging.FileHandler(self.logFile)
        logger_handler.setLevel(logging.INFO)

        # Create a Formatter for formatting the log messages
        logger_formatter = \
            logging.Formatter('%(name)s - %(levelname)s - %(message)s')

        # Add the Formatter to the Handler
        logger_handler.setFormatter(logger_formatter)

        # Add the Handler to the Logger
        self.logger.addHandler(logger_handler)
        self.logger.info(_('Entering module'))

        # ----------------
        box0 = tk.Frame(self, width=800, height=400)
        self.lbl0 = tk.Label(box0, text=_("Concatenate signals"),
                             fg="blue", font=("Helvetica", 16))
        self.lbl0.pack()
        box0.pack()

        # ------------------------------

        box2 = tk.Frame(self)

        self.file1 = tk.StringVar()
        self.file1.set("")
        lbl2file1 = tk.Label(box2, text=_("First File:"), padx=10, pady=10)
        lbl2file1.grid(row=0, column=0, padx=5, pady=5)
        ent2file1 = tk.Entry(box2, textvariable=self.file1, width=30)
        ent2file1.grid(row=0, column=1, padx=1, pady=5)

        self.file2 = tk.StringVar()
        self.file2.set("")
        lbl2file2 = tk.Label(box2, text=_("Second file:"), padx=10, pady=10)
        lbl2file2.grid(row=0, column=2, padx=5, pady=5)
        ent2file2 = tk.Entry(box2, textvariable=self.file2, width=30)
        ent2file2.grid(row=0, column=3, padx=1, pady=5)

        self.file3 = tk.StringVar()
        self.file3.set("result.csv")
        lbl2file3 = tk.Label(box2, text=_("Result file:"), padx=10, pady=10)
        lbl2file3.grid(row=0, column=4, padx=5, pady=5)
        ent2file3 = tk.Entry(box2, textvariable=self.file3, width=30)
        ent2file3.grid(row=0, column=5, padx=1, pady=5)

        self.btnFile1 = tk.Button(box2, text=_("..."),
                                  command=self.sel_file1)
        self.btnFile1.grid(row=1, column=1, padx=2, pady=2)

        self.btnFile2 = tk.Button(box2, text=_("..."),
                                  command=self.sel_file2)
        self.btnFile2.grid(row=1, column=3, padx=2, pady=2)

        self.btnFile3 = tk.Button(box2, text=_("..."),
                                  command=self.sel_file3)
        self.btnFile3.grid(row=1, column=5, padx=2, pady=2)

        # ----------------------------------------------

        box3 = tk.Frame(self, bd=2, padx=5, pady=5, relief=tk.RAISED)

        self.btnStart = tk.Button(box3, text=_("Generate and save file"),
                                  command=self.start_job)
        self.btnStart.grid(row=0, column=2, padx=2, pady=2)

        box2.pack()
        box3.pack()

        return box2

    def sel_file1(self):
        my_filetypes = [(_('csv files'), '.csv'),
                        (_('text files'), '.txt'),
                        (_('all files'), '.*')]

        msg1 = _("Please select a file name for load:")
        arquivo = tk.filedialog.askopenfilename(parent=self,
                                                initialdir=os.getcwd(),
                                                title=msg1,
                                                filetypes=my_filetypes)

        if arquivo:
            self.file1.set(arquivo)

    def sel_file2(self):
        my_filetypes = [(_('csv files'), '.csv'),
                        (_('text files'), '.txt'),
                        (_('all files'), '.*')]
        msg1 = _("Please select a file name for load:")
        arquivo = tk.filedialog.askopenfilename(parent=self,
                                                initialdir=os.getcwd(),
                                                title=msg1,
                                                filetypes=my_filetypes)

        if arquivo:
            self.file2.set(arquivo)

    def sel_file3(self):
        my_filetypes = [(_('csv files'), '.csv'),
                        (_('text files'), '.txt'),
                        (_('all files'), '.*')]

        msg1 = _("Please select a file name for load:")
        arquivo = tk.filedialog.askopenfilename(parent=self,
                                                initialdir=os.getcwd(),
                                                title=msg1,
                                                filetypes=my_filetypes)

        if arquivo:
            self.file3.set(arquivo)

    def start_job(self):
        self.vetor_pontos1 = np.loadtxt(self.file1.get(), delimiter=',')
        self.vetor_pontos2 = np.loadtxt(self.file2.get(), delimiter=',')

        self.vetor_pontos = np.concatenate((self.vetor_pontos1,
                                           self.vetor_pontos2))

        self.salva_curva()

    def salva_curva(self):
        arquivo_saida = self.file3.get()

        if arquivo_saida:
            np.savetxt(arquivo_saida, self.vetor_pontos, newline="\n")
            tk.messagebox.showinfo(_("Success"), _("File saved"))
