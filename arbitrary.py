#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  arbitrary.py
#  Copyright 2018-05-03 tavares <tavares@tavares-Inspiron-5558>
#  1.0
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#  ATENÇÂO. A versão do PILLOW DEVE SER a 3.1.2- Versões superiores
#  não estão funcionando
#
# O vetor de dados correspodente ao sinal lido é um vetor numpy
# denominado self.vetor_pontos

import tksimpledialog
import tkinter as tk
import os
import logging
import sys
from pathlib import Path
from os.path import join
import gettext
import numpy as np

_ = gettext.gettext


class Arbitrary(tksimpledialog.Dialog):

    def __init__(self, parent, title=None):
        tksimpledialog.Dialog.__init__(self, parent, title, numero_botoes=1)

    def body(self, master):
        # create dialog body.  return widget that should have
        # initial focus.  this method should be overridden

        if getattr(sys, 'frozen', False):
            # If the application is run as a bundle, the pyInstaller bootloader
            # extends the sys module by a flag frozen=True and sets the app
            # path into variable _MEIPASS'.
            application_path = sys._MEIPASS  # NOQA
        else:
            application_path = os.path.dirname(os.path.abspath(__file__))  # NOQA

        self.homeDir = str(Path.home())
        self.logFile = join(self.homeDir, "clsig1d/clsig1d.log")

        # Create the Logger
        self.logger = logging.getLogger(__name__)
        self.logger.setLevel(logging.INFO)

        # Create the Handler for logging data to a file
        logger_handler = logging.FileHandler(self.logFile)
        logger_handler.setLevel(logging.INFO)

        # Create a Formatter for formatting the log messages
        logger_formatter = \
            logging.Formatter('%(name)s - %(levelname)s - %(message)s')

        # Add the Formatter to the Handler
        logger_handler.setFormatter(logger_formatter)

        # Add the Handler to the Logger
        self.logger.addHandler(logger_handler)
        self.logger.info(_('Entering module'))

        # aqui largura e altura em pixels
        self.altura_canvas = 400
        self.larguraCanvas = 800

        # ----------------
        box0 = tk.Frame(self)
        self.lbl0 = tk.Label(box0,
                             text=_("Arbitrary function generator"),
                             fg="blue", font=("Helvetica", 16))
        self.lbl0.pack()
        box0.pack()

        box1 = tk.Frame(self, width=800, height=400)
        self.canvas = tk.Canvas(box1, bg='#FFFFFF',
                                width=self.larguraCanvas,
                                height=self.altura_canvas)
        self.canvas.config(scrollregion=self.canvas.bbox("all"))

        hbar = tk.Scrollbar(box1, orient=tk.HORIZONTAL)
        hbar.pack(side=tk.BOTTOM, fill=tk.X)
        hbar.config(command=self.canvas.xview)
        vbar = tk.Scrollbar(box1, orient=tk.VERTICAL)
        vbar.pack(side=tk.RIGHT, fill=tk.Y)
        vbar.config(command=self.canvas.yview)
        self.canvas.config(xscrollcommand=hbar.set,
                           yscrollcommand=vbar.set)
        self.canvas.pack(side=tk.LEFT, expand=True, fill=tk.BOTH)

        # evento de mouse da canvas
        self.canvas.bind("<Button-1>", self.botao_esquerdo)
        self.canvas.bind("<Button-3>", self.botao_direito)
        self.canvas.bind("<Button-4>", self.roda_aumenta)
        self.canvas.bind("<Button-5>", self.roda_diminui)
        # ------------------------------
        box2 = tk.Frame(self)
        self.points = tk.StringVar()
        self.points.set("256")
        lbl2points = tk.Label(box2, text=_("Points:"), padx=10, pady=10)
        lbl2points.grid(row=0, column=0, padx=5, pady=5)
        ent2points = tk.Entry(box2, textvariable=self.points, width=5)
        ent2points.grid(row=0, column=1, padx=1, pady=5)

        self.maxv = tk.StringVar()
        self.maxv.set("100")
        lbl2_maxv = tk.Label(box2, text=_("Max value:"),
                             padx=10, pady=10)
        lbl2_maxv.grid(row=0, column=2, padx=5, pady=5)
        ent2_maxv = tk.Entry(box2, textvariable=self.maxv, width=5)
        ent2_maxv.grid(row=0, column=3, padx=1, pady=5)

        self.minV = tk.StringVar()
        self.minV.set("-100")
        lbl2_minv = tk.Label(box2, text=_("Min value:"),
                             padx=10, pady=10)
        lbl2_minv.grid(row=0, column=4, padx=5, pady=5)
        ent2_minv = tk.Entry(box2, textvariable=self.minV, width=5)
        ent2_minv.grid(row=0, column=5, padx=1, pady=5)

        # ----------------------------------------------

        box3 = tk.Frame(self, bd=2, padx=10, pady=10, relief=tk.RAISED)

        self.btnStart = tk.Button(box3, text=_("Start Job"),
                                  command=self.start_job)
        self.btnStart.grid(row=0, column=2, padx=5, pady=5)

        self.btnSave = tk.Button(box3, text=_("Save file"),
                                 command=self.salva_curva,
                                 state=tk.DISABLED)

        self.btnSave.grid(row=0, column=3, padx=5, pady=5)

        self.btnLoad = tk.Button(box3, text=_("Load file"),
                                 command=self.le_curva)
        self.btnLoad.grid(row=0, column=4, padx=5, pady=5)

        # -----------------------------------
        box4 = tk.Frame(self)

        self.pontoAtual = tk.StringVar()
        self.pontoAtual.set("0")
        lbl4_pontoatual = tk.Label(box4, text=_("Present point:"),
                                   padx=10, pady=10)
        lbl4_pontoatual.grid(row=0, column=1, padx=5, pady=5)
        ent4_pontoatual = tk.Entry(box4, textvariable=self.pontoAtual,
                                   width=5)
        ent4_pontoatual.grid(row=0, column=2, padx=1, pady=5)

        self.valor_atual = tk.StringVar()
        self.valor_atual.set("0")
        lbl4_valoratual = tk.Label(box4, text=_("Value:"),
                                   padx=10, pady=10)
        lbl4_valoratual.grid(row=0, column=3, padx=5, pady=5)
        ent4_valoratual = tk.Entry(box4, textvariable=self.valor_atual,
                                   width=5)
        ent4_valoratual.grid(row=0, column=4, padx=1, pady=5)

        box4.pack()
        box1.pack()
        box2.pack()
        box3.pack()

        self.pontoatualn = 0
        self.valor_atualn = 0

        self.deltaValue = (float(self.maxv.get()) -
                           float(self.minV.get()))/100.0

        # estado:  0-> recebe comandos   1-> Traçando gráfico
        # em traçando gráfico o botão esquerod do mouse incrementa o pontoAtual
        #                     o botão esquerdo decremetna o pontoATual
        #                     a rodinha cresce ao diminui o valor
        self.estado = 0

        return self.canvas

    def botao_esquerdo(self, event):
        if self.estado == 1:
            if self.pontoatualn <= int(self.points.get()):
                self.pontoatualn += 1
                self.pontoAtual.set(str(self.pontoatualn))
                self.show_curve(self.vetor_pontos)

    def botao_direito(self, event):
        if self.estado == 1:
            if self.pontoatualn > 0:
                self.pontoatualn -= 1
                self.pontoAtual.set(str(self.pontoatualn))
                self.show_curve(self.vetor_pontos)

    def roda_aumenta(self, event):
        if self.estado == 1:
            if self.valor_atualn < float(self.maxv.get()):
                self.valor_atualn += self.deltaValue

                if self.valor_atualn > float(self.maxv.get()):
                    self.valor_atualn = float(self.maxv.get())

            self.valor_atual.set(str(self.valor_atualn))

            self.vetor_pontos[self.pontoatualn] = self.valor_atualn
            self.show_curve(self.vetor_pontos)

    def roda_diminui(self, event):
        if self.estado == 1:
            if self.valor_atualn > float(self.minV.get()):
                self.valor_atualn -= self.deltaValue

                if self.valor_atualn < float(self.minV.get()):
                    self.valor_atualn = float(self.minV.get())

            self.valor_atual.set(str(self.valor_atualn))
            self.vetor_pontos[self.pontoatualn] = self.valor_atualn
            self.show_curve(self.vetor_pontos)

    def start_job(self):
        """
        inicializa o vetor de pontos
        inicializa a variavel pontoAtual
        inicializa a variavel valorAtual
        """
        self.vetor_pontos = np.zeros(int(self.points.get()))
        self.pontoatualn = 0
        self.valor_atualn = 0

        self.btnSave.config(state=tk.NORMAL)
        self.canvas.delete("all")
        self.estado = 1
        self.show_curve(self.vetor_pontos)

    def salva_curva(self):
        # Ask the user to select a single file name for saving.
        my_filetypes = [(_('csv files'), '.csv'),
                        (_('text files'), '.txt'),
                        (_('all files'), '.*')]

        msgt = _("Please select a file name for saving:")
        arquivo = tk.filedialog.asksaveasfilename(parent=self,
                                                  initialdir=os.getcwd(),
                                                  title=msgt,
                                                  filetypes=my_filetypes)

        if arquivo:
            np.savetxt(arquivo, self.vetor_pontos, newline="\n")

    def le_curva(self):
        # Ask the user to select a single file name for saving.
        my_filetypes = [(_('csv files'), '.csv'),
                        (_('text files'), '.txt'),
                        (_('all files'), '.*')]

        msgt = _("Please select a file name for load:")
        arquivo = tk.filedialog.askopenfilename(parent=self,
                                                initialdir=os.getcwd(),
                                                title=msgt,
                                                filetypes=my_filetypes)

        if arquivo:
            self.vetor_pontos = np.loadtxt(arquivo, delimiter=',')
            vmax = np.amax(self.vetor_pontos)
            vmin = np.amin(self.vetor_pontos)
            tamanho = self.vetor_pontos.size

            self.points.set(str(tamanho))
            self.maxv.set(str(vmax))
            self.minV.set(str(vmin))

            self.pontoatualn = 0
            self.valor_atualn = 0

            self.btnSave.config(state=tk.NORMAL)

            self.estado = 1
            self.show_curve(self.vetor_pontos)

    def show_curve(self, list_vector):
        """
        list_vector é um array numpy
        """
        tamanho = list_vector.size
        n_loop = 0

        while n_loop < tamanho:
            self.traca_linhavertical(n_loop, list_vector[n_loop])
            n_loop += 1

    def traca_linhavertical(self, x1, y1):
        """
        x1 é um número inteiro correspondente ao ponto de amostragem
        y1 é um valor float corresponde ao valor do eixo y neste ponto
        """
        max_vn = float(self.maxv.get())
        min_vn = float(self.minV.get())
        a = self.altura_canvas / (min_vn - max_vn)
        # se estiver sobre a linha zero marca apenas a linha com y=1
        if y1 == 0.0:
            self.canvas.create_oval(x1, self.altura_canvas/2.0, x1,
                                    self.altura_canvas/2.0 + 1,
                                    fill="red")
        else:
            float_y = a * y1 - a * min_vn + self.altura_canvas

            int_y = int(float_y)

            self.canvas.create_line(x1, 0, x1, self.altura_canvas,
                                    fill="white")

            if x1 == self.pontoatualn:
                self.canvas.create_line(x1, self.altura_canvas / 2.0, x1,
                                        int_y, fill="red")
            else:
                self.canvas.create_line(x1, self.altura_canvas / 2.0, x1,
                                        int_y, fill="blue")
